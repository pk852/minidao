package minidao.test;



import java.util.List;

import javax.annotation.Resource;

import minidao.test.dao.PersonDao;
import minidao.test.entity.Person;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/applicationContext.xml")
@Transactional
public class PersonDaoTest {
@Resource
PersonDao personDao;

	@Test
	@Rollback(false)
	public void testAdd() {
		Person person = new Person();
		person.setId(4);
		person.setNameString("test4");
		personDao.add(person);
	}

	@Test
	public void testDel() {
		 
	}

	@Test
	public void testUpdate() {
		Person person = new Person();
		person.setId(1);
		person.setNameString("update");
		personDao.update(person);
	}

	@Test
	public void testGet() {
		Person person = personDao.get();
		System.out.println(person);
	}

	@Test
	public void testGetAll() {
		 List<Person> list = personDao.getAll();
		 System.out.println(list.size());
	}

}

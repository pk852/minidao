package com.laj.test;

import javax.annotation.Resource;

import minidao.test.entity.Person;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.laj.dao.MyBaseDao;
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/applicationContext.xml")

public class ConfigTest {
	@Resource(name="myBaseDao")
	MyBaseDao myBaseDao;

	@Test
	@Rollback(false)
	@Transactional
	public void test() {
		System.out.println(myBaseDao==null);
		Person p = new Person();
		p.setId(8);
		p.setNameString("12345678");
		myBaseDao.add(p);
	}

}

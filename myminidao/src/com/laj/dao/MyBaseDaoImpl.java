package com.laj.dao;



import javax.annotation.Resource;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

public class MyBaseDaoImpl implements MyBaseDao {

	private SessionFactory sessionFactory;

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	@Resource(name = "sessionFactory")
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	protected Session getSession() {
		return sessionFactory.getCurrentSession();
	}

	@Override
	public <T> void add(T t) {

		getSession().save(t);

	}

	@Override
	public <T> void del(T t) {
		getSession().delete(t);
	}

	@Override
	public <T> void update(T t) {
		getSession().update(t);
	}

}

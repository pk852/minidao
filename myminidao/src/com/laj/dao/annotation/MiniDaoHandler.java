package com.laj.dao.annotation;

import java.beans.BeanInfo;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;

import org.springframework.jdbc.core.JdbcTemplate;

import com.laj.dao.MyBaseDao;

public class MiniDaoHandler implements MethodInterceptor {

	MyBaseDao myBaseDao;

	JdbcTemplate jdbcTemplate;

	@Override
	public Object invoke(MethodInvocation methodInvocation) throws Throwable {
		Method method = methodInvocation.getMethod();
		Object[] args = methodInvocation.getArguments();

		// 不是抽象方法
		if (!Modifier.isAbstract(method.getModifiers())) {
			return methodInvocation.proceed();
		}
		// 是抽象方法
		// 1.是否是增删改操作
		if (myBaseDaoHandler(method, args)) {
			return null;
		}

		if (method.isAnnotationPresent(MySql.class)) {
			MySql mySql = method.getAnnotation(MySql.class);
			String sqlString = mySql.sql();

			Class<?> returnType = method.getReturnType();
			// 2.查询操作
			if (returnType.isPrimitive()) {
				// 返回类型是基本类型
				Number resultNumber = jdbcTemplate.queryForObject(sqlString,
						BigDecimal.class);
				if (returnType.toString().equals("int")) {
					return resultNumber.intValue();
				} else if (returnType.toString().equals("long")) {
					return resultNumber.longValue();

				} else if (returnType.toString().equals("float")) {
					return resultNumber.floatValue();
				} else if (returnType.toString().equals("double")) {
					return resultNumber.doubleValue();
				}

			} else if (returnType.isAssignableFrom(List.class)) {
				// 返回类型是集合
				Class<?> returnTypeClass = Class.forName(mySql.entityClassName());
				List<Map<String, Object>> queryResult = jdbcTemplate
						.queryForList(sqlString);
				List<Object> result = new ArrayList<Object>();
				if (queryResult.size() > 0) {
					for (Map<String, Object> resultMap : queryResult) {
						Object resultObject = convertMapToObject(resultMap,
								returnTypeClass);
						result.add(resultObject);
					}
				}
				return result;
			} else if (returnType.isAssignableFrom(String.class)) {
				return jdbcTemplate.queryForObject(sqlString, String.class);
			} else if (returnType.isAssignableFrom(Void.class)) {
				jdbcTemplate.execute(sqlString);
				return "";
			} else {
				// 返回类型是自定义类型
				Class<?> returnTypeClass = Class.forName(mySql.entityClassName());
				Map<String, Object> queryResult = jdbcTemplate
						.queryForMap(sqlString);

				Object resultObject = convertMapToObject(queryResult,
						returnTypeClass);

				return resultObject;
			}

		}

		return null;
	}
	/**
	 * 将一个map封装成实体类
	 * @param resultMap
	 * @param returnTypeClass
	 * @return
	 * @throws IntrospectionException
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 * @throws IllegalArgumentException
	 * @throws InvocationTargetException
	 */
	private Object convertMapToObject(Map<String, Object> resultMap,
			Class<?> returnTypeClass) throws IntrospectionException,
			InstantiationException, IllegalAccessException,
			IllegalArgumentException, InvocationTargetException {
		Object resultObject = null;
		BeanInfo beanInfo = Introspector.getBeanInfo(returnTypeClass);
		PropertyDescriptor[] propertys = beanInfo.getPropertyDescriptors();
		if (propertys != null) {
			resultObject = returnTypeClass.newInstance();
			for (PropertyDescriptor propertyDescriptor : propertys) {
				if (resultMap.containsKey(propertyDescriptor.getName())) {
					propertyDescriptor.getWriteMethod().invoke(resultObject,
							resultMap.get(propertyDescriptor.getName()));
				}
			}
		}
		return resultObject;
	}

	/**
	 * 根据方法判断结果类型
	 * 
	 * @param method
	 * @param args
	 * @return
	 */
	private boolean myBaseDaoHandler(Method method, Object[] args) {
		String methodNameString = method.getName();
		boolean result = false;
		switch (methodNameString) {
		case "add":
			myBaseDao.add(args[0]);
			result = true;
			break;
		case "del":
			myBaseDao.del(args[0]);
			result = true;
			break;
		case "update":
			myBaseDao.update(args[0]);
			result = true;
			break;

		default:
			result = false;
			break;
		}
		return result;
	}

	public MyBaseDao getMyBaseDao() {
		return myBaseDao;
	}

	public void setMyBaseDao(MyBaseDao myBaseDao) {
		this.myBaseDao = myBaseDao;
	}

	public JdbcTemplate getJdbcTemplate() {
		return jdbcTemplate;
	}

	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

}

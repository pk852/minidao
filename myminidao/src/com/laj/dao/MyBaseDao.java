package com.laj.dao;



public interface MyBaseDao {
	/**
	 * 添加方法
	 * @param t
	 */
	public <T> void add(T t);
	/**
	 * 删除方法
	 * @param id
	 */
	public <T> void del(T id);
	/**
	 * 修改方法
	 * @param t
	 */

	public <T> void update(T t);
 

}

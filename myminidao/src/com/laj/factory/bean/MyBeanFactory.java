package com.laj.factory.bean;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.springframework.aop.framework.ProxyFactoryBean;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;

import com.laj.dao.annotation.MiniDao;

import com.laj.factory.util.PackagesToScanUtil;

public class MyBeanFactory implements BeanFactoryPostProcessor {
	private List<String> packagesToScan = new ArrayList<String>();

	/**
	 * 用于扫描、解析有MiniDao注释的类，并为其增加miniDaoHandler拦截器
	 */
	/**
	 * 1、 在该类中循环传入的minidao配置项，解析配置项查出该配置项对应文件夹下的所有class文件并进行加载
	 * 2.循环这些class文件类，判断是否是以MiniDao为注解方式，如果是则单独加载一个接口的代理类，
	 * 将spring容器传入改代理类，并将miniDaoHandler作为拦截器传入，将对应的类注入给spring代理工厂管理，
	 * 也就是说当这些类的方法在被调用的时候，都会去动态加载miniDaoHandler中的invoke方法。
	 */
	@Override
	public void postProcessBeanFactory(
			ConfigurableListableBeanFactory beanFactory) throws BeansException {

		if (packagesToScan.size() > 0) {
			for (String packageString : packagesToScan) {
				Set<Class<?>> clazzs = PackagesToScanUtil
						.getClasses(packageString);
				for (Class<?> mxDaoClass : clazzs) {
					if (mxDaoClass.isAnnotationPresent(MiniDao.class)) {
						ProxyFactoryBean factoryBean = new ProxyFactoryBean();
						// 将spring容器传入该代理类
						factoryBean.setBeanFactory(beanFactory);
						factoryBean.setInterfaces(new Class[] { mxDaoClass });
						factoryBean
								.setInterceptorNames(new String[] { "miniDaoHandler" });

						String beanName = getFirstSmall(mxDaoClass
								.getSimpleName());
						// 是否被spring管理
						if (!beanFactory.containsBean(beanName)) {
							// 则将该bean和它的代理工厂注入到bean工厂中
							beanFactory.registerSingleton(beanName,
									factoryBean.getObject());
						}
					}
				}
			}
		}
	}

	public List<String> getPackagesToScan() {
		return packagesToScan;
	}

	public void setPackagesToScan(List<String> packagesToScan) {
		this.packagesToScan = packagesToScan;
	}

	public String getFirstSmall(String name) {
		name = name.trim();
		if (name.length() >= 2) {
			return name.substring(0, 1).toLowerCase() + name.substring(1);
		} else {
			return name.toLowerCase();
		}

	}
}
